# Datatables

A package to help with paginating/filtering and sorting tables of data.

###Installation

Add Datatables to your composer.json file:

```
    "require": {
        "php": ">=5.5.9",
        "laravel/framework": "5.1.*",
        "fifteen/datatables": "1.0.*"
    },
```

Add minimum-stability: dev:
```sh
    "minimum-stability": "dev",
```

As the repository isn't on Packagist, you also need to include it in the repositories list:

```php
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:fifteendigital/fifteen-datatables.git"
        }
    ]
```

Run composer update:

```sh
> composer update
```

### Create datatable object

Create a datatable object in your controller:

```
use Fifteen\DataTables\RepositoryDataTableController as DataTable;

class GroupsController extends Controller {

    public function index()
    {
        $data_table = new DataTable($this->service->getRepository(), 'groups.index', Input::all());
        $records = $data_table->getPaginated();

        return View::make('screens.groups.index', compact('data_table', 'records'));
    }
}
```

### Rendering datatable controls in the view

Column headings:
```sh
{!! $data_table->sortBy('name', Alang::get('general.name')) !!}
```

Select number of records to display on page:
```sh
{!! Form::select('records', 
    $data_table->getRecordsPerPageOptions(), 
    $data_table->getRecordsPerPage(), 
    ['size' => 1, 'class' => 'form-control input-xsmall form-inline']) 
!!}
```

Search box:
```sh
{!! Form::text('search', $data_table->getSearchString(), ['size' => 1, 'class' => 'form-control input-medium input-inline']) !!}
```

Records displayed:
```sh
Showing {{ $records->firstItem() }} to {{ $records->lastItem() }} of {{ $records->total() }} entries
```

Pagination:
```sh
{!! $records->appends(Request::except('page'))->render(new \Fifteen\DataTables\Presenters\MetronicPresenter($records)) !!}

### View example

```html
<div class="row hidden-print">
    <div class="col-md-6 col-sm-12">
        <div class="dataTables_length">
            {!! Form::open(['method' => 'GET', 'class' => 'form-inline']) !!}
            <label>
                {!! Form::select('records', $data_table->getRecordsPerPageOptions(), $data_table->getRecordsPerPage(), ['size' => 1, 'class' => 'form-control input-xsmall form-inline']) !!}
                <span class="">{{ strtolower(Alang::get('general.records')) }}</span>
            </label>
                
            {!! Form::close() !!}
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="dataTables_filter pull-right">
            {!! Form::open(['method' => 'GET']) !!}
            <label>
                {{ Alang::get('general.search') }}: 
                {!! Form::text('search', $data_table->getSearchString(), ['size' => 1, 'class' => 'form-control input-medium input-inline']) !!}
            </label>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@if ($records->count())
    <div class="table-responsive">
        <table class="table dataTable table-striped table-hover table-bordered flip-content">
            <thead>
                <tr class="sort-header">
                    {!! $data_table->sortBy('name', Alang::get('general.name')) !!}
                    <th class="text-center">{{ Alang::get('general.actions') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($records as $record)
                    <tr>
                        <td>{{ $record->name }}</td>
                        <td class="text-center">
                            <a class="btn btn-xs purple" 
                                href="{{ route('groups.show', $record->id) }}" 
                                title="{{ Alang::get('general.view_record') }}">
                                <i class="fa fa-search"></i>
                                {{ Alang::get('general.view') }}
                            </a>
                            &ensp;
                            <a class="btn btn-xs blue" 
                                href="{{ route('groups.edit', $record->id) }}"
                                title="{{ Alang::get('general.edit_record') }}">
                                <i class="fa fa-pencil"></i>
                                {{ Alang::get('general.edit') }}
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    
    @if ($records->lastPage() > 1)
        <div class="row data_table_controls">
            <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="sample_2_info">
                    {{ Alang::get('general.showing') }} {{ $records->firstItem() }} 
                        {{ strtolower(Alang::get('general.to')) }} {{ $records->lastItem() }}
                        {{ strtolower(Alang::get('general.of')) }}  {{ $records->total() }} 
                        {{ strtolower(Alang::get('general.entries')) }} 
                </div>
            </div>
            <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging-bootstrap hidden-print">
                    <ul class="pagination">
                        {!! $records->appends(Request::except('page'))->render(new \Fifteen\DataTables\Presenters\MetronicPresenter($records)) !!}
                    </ul>
                </div>
            </div>
        </div>
    @endif 

@else
    <p>{{ Alang::get('general.there_are_currently_no_records') }}. {!! link_to_route("groups.create", Alang::get('general.create_a_new_record')) !!}.</p>
@endif
```

### Built-in DataTableControllers

There are currently two inbuilt DataTableControllers: 

```php
$repo = new GroupsRepository();
$data_table = new RepositoryDataTableController($repo, $route, $input, $defaults);

$query = User::where('name', 'like', 's%');
$data_table = new QueryDataTableController($query, $route, $input, $defaults);
```
