<?php

namespace Fifteen\DataTables;

use Illuminate\Database\QueryException;

class QueryDataTableController extends BaseDataTableController {

    protected $query;

    public function __construct($query, $route, $input = [], $defaults = [])
    {
        $this->query = $query;
        parent::__construct($route, $input, $defaults);
    }

    public function getFilteredQuery()
    {
        // dd($this->query->getQuery()->getQuery());
        $query = clone($this->query);
        if (!empty($this->search)) {
            $query = $query->search($this->search);
        }
        if (!empty($this->filter)) {
            $query = $query->filter($this->filter);
        }
        if (!empty($this->sortBy) && !empty($this->direction)) {
            $query = $query->orderBy($this->sortBy, $this->direction);
        }
        return $query;
    }

    public function getPaginated()
    {
        $query = $this->getFilteredQuery();
        try {
            $records = $query->paginate($this->records);
        } catch (QueryException $e) {
            // order by clause can throw error if hacked in url - this doesn't happen with the RepositoryDataTableController
            $query = clone($this->query);   // This doesn't actually work - it doesn't seem to deep-clone objects (possibly something to to with the IOC container?)
            $query->getQuery()->orders = null;  // This clears out the order by clause if an error is thrown (a bit hacky!)
            $records = $query->paginate($this->records);
        }
        // overwrite default path for links, as it appends a slash on the end
        $records->setPath($this->getUrl());

        return $records;
    }

    public function getAll()
    {
        $query = $this->query;
        if (!empty($this->search)) {
            $query = $query->search($this->search);
        }
        if (!empty($this->filter)) {
            $query = $query->filter($this->filter);
        }
        if (!empty($this->sortBy) && !empty($this->direction)) {
            $query = $query->orderBy($this->sortBy, $this->direction);
        }
        $records = $query->get();

        // overwrite default path for links, as it appends a slash on the end
        return $records;
    }

    public function getQuery()
    {
        return $this->query;
    }

}
