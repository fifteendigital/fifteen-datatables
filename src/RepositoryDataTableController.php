<?php namespace Fifteen\DataTables;

class RepositoryDataTableController extends BaseDataTableController {

	protected $repo;

	public function __construct($repo, $route, $input = [], $defaults = [])
	{
		$this->repo = $repo;
		parent::__construct($route, $input, $defaults);
	}

	public function getPaginated()
	{
		$this->repo->take($this->records);
		if (!empty($this->search)) {
			$this->repo->searchFor($this->search);
		}
		if (!empty($this->filter)) {
			$this->repo->filter($this->filter);
		}
		if (!empty($this->sortBy) && !empty($this->direction)) {
			$this->repo->sortBy($this->sortBy, $this->direction);
		}
		$records = $this->repo->paginate($this->records);

		// overwrite default path for links, as it appends a slash on the end
		$records->setPath($this->getUrl());

		return $records;
	}

	public function getAll()
    {
		// $this->repo->take($this->records);
		if (!empty($this->search)) {
			$this->repo->searchFor($this->search);
		}
		if (!empty($this->filter)) {
			$this->repo->filter($this->filter);
		}
		if (!empty($this->sortBy) && !empty($this->direction)) {
			$this->repo->sortBy($this->sortBy, $this->direction);
		}
        $records = $this->repo->make()->get();

        // overwrite default path for links, as it appends a slash on the end
        return $records;
    }
}
