<?php namespace Fifteen\DataTables;

interface DataTableInterface {

    function getPaginated();

	function getAll();

	function getRecordsPerPageOptions();

	function getRecordsPerPage();

	function getSortBy();

	function getDirection();
	
	function getSearchString();

}