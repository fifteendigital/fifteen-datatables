<?php namespace Fifteen\DataTables\Repositories;

use Illuminate\Support\Facades\Session;

class DataTableSessionsRepository 
{
	protected $key;

	public function __construct($key)
	{
		$this->setKey($key);
	}

	public function setKey($key) 
	{
		$this->key = $key;
	}

	public function put($data)
	{
		Session::put($this->key, $data);
	}

	public function get()
	{
		return Session::get($this->key);
	}

}