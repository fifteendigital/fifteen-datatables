<?php namespace Fifteen\DataTables;

use Illuminate\Support\Facades\Request;
use Fifteen\DataTables\Repositories\DataTableSessionsRepository;

abstract class BaseDataTableController implements DataTableInterface
{
	protected $fields = ['records', 'sortBy', 'direction', 'search', 'filter'];
	protected $route;
	protected $records = 100;
	protected $sortBy = '';
	protected $direction = '';
	protected $search = '';
	protected $filter = [];
	protected $input = [];
	protected $store;

	public function __construct($route, $input = [], $defaults = [])
	{
		$this->route = $route;
		$this->store = new DataTableSessionsRepository($this->getKey());
		foreach ($defaults as $key => $value) {
			$this->$key = $value;
		}
		foreach ($this->getStoredSettings() as $key => $value) {
			$this->$key = $value;
		}
		$this->saveSettings($input);
		$this->input = $input;

	}

	public function getKey()
	{
        if (is_array($this->route)) {
            $route_params = $this->route;
            $route = array_shift($route_params);
            return route($route, $route_params);
        } else {
            return route($this->route);
        }
	}

    public function getUrl($params = [])
	{
		if (is_array($this->route)) {
            $route_params = $this->route;
            $route = array_shift($route_params);
            $route_params += $params;
			$url = route($route, $route_params);
		} else {
			$url = route($this->route, $params);
		}
		return $url;
	}

	public function getStoredSettings()
	{
		//$key = $this->getKey();
		//$defaults = \Session::get($key);
		$defaults = $this->store->get();
		//pd($defaults);
		$stored = [];
		foreach ($this->fields as $item) {
			if (!empty($defaults[$item])) {
				$stored[$item] = $defaults[$item];
			}
		}

		return $stored;
	}

	public function saveSettings(array $params)
	{
		$data = [];
		foreach ($this->fields as $item) {
			if (isset($params[$item])) {
				$this->$item = $params[$item];
			}
			$data[$item] = $this->$item;
		}
		//$key = $this->getKey();
		//\Session::put($key, $data);
		$this->store->put($data);
	}

	public abstract function getPaginated();

	public function sortBy($column, $body, $attributes = [])
	{
		$params = $this->input;
		$direction = $this->getSortDirection($column);
		$class = $this->getSortClass($column);
		if ( ! empty($attributes['class'])) {
			$attributes['class'] .= ' ' . $class;
		} else {
			$attributes['class'] = $class;
		}
		$params['sortBy'] = $column;
		$params['direction'] = $direction;
		$url = $this->getUrl($params);
		$attribute_string = '';
		foreach ($attributes as $key => $value) {
			$attribute_string .= ' ' . $key . '="' . $value . '"';
		}
		return '<th' . $attribute_string . ' data-url="' . $url . '">' . $body . "</th>";
	}

	public function getSortClass($column)
	{
		if ($this->sortBy == $column)  {
			if ($this->direction == 'asc') {
				return 'sorting_asc';
			} else {
				return 'sorting_desc';
			}
		} else {
			return 'sorting';
		}
	}

	public function getSortDirection($column)
	{
		if ($this->sortBy == $column and $this->direction == 'asc') {
			return 'desc';
		} else {
			return 'asc';
		}
	}
	public function getParams()
	{
		$params = [
			'sortBy' => empty($this->sortBy) ? '' : $this->sortBy,
			'direction' => empty($this->direction) ? '' : $this->direction,
			'records' => empty($this->records) ? '' : $this->records
		];
		return $params;
	}
	public function getRecordsPerPageOptions()
	{
		$options = [5, 10, 20, 50, 100, 1000];
		return array_combine($options, $options);
	}
	public function getRecordsPerPage()
	{
		return $this->records;
	}
	public function getSortBy()
	{
		return $this->sortBy;
	}
	public function getDirection()
	{
		return $this->direction;
	}
	public function getSearchString()
	{
		return $this->search;
	}
	public function setFilter($filter)
	{
		$this->saveSettings(['filter' => $filter]);
		$this->filter = $filter;
	}
	public function getFilter()
	{
		return array_filter($this->filter, function($value) {
            return $value !== '';
        });
	}
}
