<?php namespace Fifteen\DataTables\Presenters;

class MetronicPresenter extends \Illuminate\Pagination\BootstrapThreePresenter
{

    /**
     * Get HTML wrapper for disabled text.
     *
     * @param  string  $text
     * @return string
     */
    public function getDisabledTextWrapper($text)
    {
        return '<li class="disabled"><span>' . $text . '</span></li>';
    }

    /**
     * Get HTML wrapper for active text.
     *
     * @param  string  $text
     * @return string
     */
    public function getActivePageWrapper($text)
    {
        return '<li class="active"><a href="#">' . $text . '</a></li>';
    }


}